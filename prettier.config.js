module.exports = {
    experimentalTernaries: false, // 是否支持实验性的三元运算符格式化
    printWidth: 80, // 每行的最大宽度
    tabWidth: 4, // 缩进的空格数
    useTabs: false, // 是否使用制表符进行缩进
    semi: true, // 是否在语句末尾添加分号
    singleQuote: false, // 是否使用单引号而不是双引号
    quoteProps: "as-needed", // 对象属性何时需要加引号：always（总是）、as-needed（必要时）、consistent（保持一致）
    jsxSingleQuote: false, // JSX 中是否使用单引号而不是双引号
    trailingComma: "all", // 尾随逗号的使用：none（不使用）、es5（仅在 ES5 支持的场合使用）、all（所有场合）
    bracketSpacing: true, // 对象字面量中的大括号是否有空格：true（有空格）、false（无空格）
    bracketSameLine: false, // 多行对象字面量的起始大括号是否在同一行
    arrowParens: "always", // 箭头函数参数是否始终使用括号：always（始终使用）、avoid（避免使用）
    rangeStart: 0, // 格式化范围的起始位置
    proseWrap: "preserve", // 文本段落的换行方式：always（总是换行）、never（从不换行）、preserve（保留原有换行）
    htmlWhitespaceSensitivity: "css", // HTML 中空白字符的敏感度：ignore（忽略）、strict（严格）、css（CSS 标准）
    vueIndentScriptAndStyle: false, // Vue 单文件组件中 <script> 和 <style> 标签是否使用缩进
    endOfLine: "lf", // 行尾结束符：lf（Unix 风格）、crlf（Windows 风格）、auto（自动检测）
    embeddedLanguageFormatting: "auto", // 嵌入语言的格式化：auto（自动）、off（关闭）
    singleAttributePerLine: false, // 每个属性是否独占一行
    plugins: [], // 使用的插件列表
};
