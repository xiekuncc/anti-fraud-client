// 'use strict' 模式可以提高代码的安全性，帮助捕获一些常见的编码错误
"use strict";

// 导入必要的模块
const utils = require("./utils");
const webpack = require("webpack");
const config = require("../config");
const merge = require("webpack-merge");
const path = require("path");
const baseWebpackConfig = require("./webpack.base.conf");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const FriendlyErrorsPlugin = require("friendly-errors-webpack-plugin");
const pathfinder = require("portfinder");

// 获取环境变量中的主机和端口
const HOST = process.env.HOST;
const PORT = process.env.PORT && Number(process.env.PORT);

// 开发环境的 Webpack 配置
const devWebpackConfig = merge(baseWebpackConfig, {
    module: {
        rules: utils.styleLoaders({
            sourceMap: config.dev.cssSourceMap, // 启用 CSS 源代码映射
            usePostCSS: true, // 使用 PostCSS
        }),
    },
    // 选择适合开发环境的 source map 类型
    devtool: config.dev.devtool,

    // 配置开发服务器
    devServer: {
        clientLogLevel: "warning", // 设置客户端日志级别
        historyApiFallback: {
            rewrites: [
                {
                    from: /.*/,
                    to: path.posix.join(
                        config.dev.assetsPublicPath,
                        "index.html",
                    ),
                }, // 重写所有请求到 index.html
            ],
        },
        hot: true, // 启用热更新
        contentBase: false, // 由于使用了 CopyWebpackPlugin，不需要设置 contentBase
        compress: true, // 启用 gzip 压缩
        host: HOST || config.dev.host, // 设置主机地址
        port: PORT || config.dev.port, // 设置端口号
        open: config.dev.autoOpenBrowser, // 是否自动打开浏览器
        overlay: config.dev.errorOverlay
            ? { warnings: false, errors: true } // 在页面上显示错误信息
            : false,
        publicPath: config.dev.assetsPublicPath, // 设置公共路径
        proxy: config.dev.proxyTable, // 设置代理表
        quiet: true, // 静默模式，不输出编译信息
        disableHostCheck: true, // 禁用主机检查
        watchOptions: {
            poll: config.dev.poll, // 检查文件变化的频率
        },
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": require("../config/dev.env"), // 定义环境变量
        }),
        new webpack.HotModuleReplacementPlugin(), // 启用热模块替换
        new webpack.NamedModulesPlugin(), // HMR 显示正确的文件名
        new webpack.NoEmitOnErrorsPlugin(), // 发生错误时不输出资源
        // 生成 HTML 文件
        new HtmlWebpackPlugin({
            filename: "index.html", // 生成的文件名
            template: "index.html", // 模板文件
            inject: true, // 自动注入脚本和链接
        }),
        // 复制静态资源
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, "../static"), // 源目录
                to: config.dev.assetsSubDirectory, // 目标目录
                ignore: [".*"], // 忽略隐藏文件
            },
        ]),
    ],
});

// 返回一个 Promise，用于动态设置端口号
module.exports = new Promise((resolve, reject) => {
    pathfinder.basePort = process.env.PORT || config.dev.port; // 设置基础端口号
    pathfinder.getPort((err, port) => {
        if (err) {
            reject(err); // 如果获取端口号失败，拒绝 Promise
        } else {
            // 设置新的端口号
            process.env.PORT = port;
            devWebpackConfig.devServer.port = port;

            // 添加 FriendlyErrorsPlugin 插件
            devWebpackConfig.plugins.push(
                new FriendlyErrorsPlugin({
                    compilationSuccessInfo: {
                        messages: [
                            `Your application is running here: http://${devWebpackConfig.devServer.host}:${port}`,
                        ], // 成功编译时的信息
                    },
                    onErrors: config.dev.notifyOnErrors
                        ? utils.createNotifierCallback() // 如果需要通知错误，使用回调函数
                        : undefined,
                }),
            );

            resolve(devWebpackConfig); // 解决 Promise 并返回配置
        }
    });
});
