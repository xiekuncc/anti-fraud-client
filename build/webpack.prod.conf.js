// 'use strict' 模式可以提高代码的安全性，帮助捕获一些常见的编码错误
"use strict";

// 导入必要的模块
const path = require("path");
const utils = require("./utils");
const webpack = require("webpack");
const config = require("../config");
const merge = require("webpack-merge");
const baseWebpackConfig = require("./webpack.base.conf");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const OptimizeCSSPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");

// 根据环境变量选择配置文件
const env =
    process.env.NODE_ENV === "testing"
        ? require("../config/test.env")
        : require("../config/prod.env");

// 生产环境的 Webpack 配置
const webpackConfig = merge(baseWebpackConfig, {
    module: {
        rules: utils.styleLoaders({
            sourceMap: config.build.productionSourceMap, // 启用生产环境的源代码映射
            extract: true, // 提取 CSS 到单独的文件
            usePostCSS: true, // 使用 PostCSS
        }),
    },
    devtool: config.build.productionSourceMap ? config.build.devtool : false, // 根据配置决定是否启用源代码映射

    // 输出配置
    output: {
        path: config.build.assetsRoot, // 输出目录
        filename: utils.assetsPath("js/[name].[chunkhash].js"), // 生成的 JavaScript 文件名
        chunkFilename: utils.assetsPath("js/[id].[chunkhash].js"), // 生成的代码分割块文件名
    },

    // 插件配置
    plugins: [
        // 定义环境变量
        new webpack.DefinePlugin({
            "process.env": env,
        }),

        // 压缩 JavaScript
        new UglifyJsPlugin({
            uglifyOptions: {
                compress: {
                    warnings: false, // 不显示压缩警告
                },
            },
            sourceMap: config.build.productionSourceMap, // 启用源代码映射
            parallel: true, // 并行压缩
        }),

        // 提取 CSS 到单独的文件
        new ExtractTextPlugin({
            filename: utils.assetsPath("css/[name].[contenthash].css"), // 生成的 CSS 文件名
            allChunks: true, // 提取所有代码块中的 CSS
        }),

        // 压缩提取的 CSS
        new OptimizeCSSPlugin({
            cssProcessorOptions: config.build.productionSourceMap
                ? { safe: true, map: { inline: false } } // 启用源代码映射
                : { safe: true }, // 不启用源代码映射
        }),

        // 生成带有正确资产哈希的 dist/index.html
        new HtmlWebpackPlugin({
            filename:
                process.env.NODE_ENV === "testing"
                    ? "index.html" // 测试环境下的文件名
                    : config.build.index, // 生产环境下的文件名
            template: "index.html", // 模板文件
            inject: true, // 自动注入脚本和链接
            minify: {
                removeComments: true, // 移除注释
                collapseWhitespace: true, // 压缩空白
                removeAttributeQuotes: true, // 移除属性引号
            },
            chunksSortMode: "dependency", // 按依赖关系排序代码块
        }),

        // 保持模块 ID 稳定
        new webpack.HashedModuleIdsPlugin(),

        // 启用作用域提升
        new webpack.optimize.ModuleConcatenationPlugin(),

        // 将 vendor JS 分离到自己的文件中
        new webpack.optimize.CommonsChunkPlugin({
            name: "vendor",
            minChunks: (module) =>
                module.resource &&
                /\.js$/.test(module.resource) &&
                module.resource.indexOf(
                    path.join(__dirname, "../node_modules"),
                ) === 0,
        }),

        // 将 webpack 运行时和模块清单分离到单独的文件中
        new webpack.optimize.CommonsChunkPlugin({
            name: "manifest",
            minChunks: Infinity,
        }),

        // 提取共享代码块到单独的文件
        new webpack.optimize.CommonsChunkPlugin({
            name: "app",
            async: "vendor-async",
            children: true,
            minChunks: 3,
        }),

        // 复制自定义静态资源
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, "../static"), // 源目录
                to: config.build.assetsSubDirectory, // 目标目录
                ignore: [".*"], // 忽略隐藏文件
            },
        ]),
    ],
});

// 如果配置中启用了 Gzip 压缩
if (config.build.productionGzip) {
    const CompressionWebpackPlugin = require("compression-webpack-plugin");
    webpackConfig.plugins.push(
        new CompressionWebpackPlugin({
            asset: "[path].gz[query]", // 生成的 Gzip 文件名
            algorithm: "gzip", // 使用的压缩算法
            test: new RegExp(
                "\\.(" + config.build.productionGzipExtensions.join("|") + ")$",
            ), // 需要压缩的文件类型
            threshold: 10240, // 文件大小阈值
            minRatio: 0.8, // 压缩比阈值
        }),
    );
}

// 如果配置中启用了 bundle 分析报告
if (config.build.bundleAnalyzerReport) {
    const BundleAnalyzerPlugin =
        require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
    webpackConfig.plugins.push(new BundleAnalyzerPlugin());
}

// 导出配置
module.exports = webpackConfig;
