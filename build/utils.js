"use strict";

// 引入所需的模块
const path = require("path"); // 用于处理和转换文件路径
const config = require("../config"); // 引入项目的配置文件
const ExtractTextPlugin = require("extract-text-webpack-plugin"); // 用于将 CSS 提取到独立文件中
const packageConfig = require("../package.json"); // 用于获取项目的 package.json 配置

// 根据当前环境（开发或生产）返回资源路径
exports.assetsPath = function (_path) {
    const assetsSubDirectory =
        process.env.NODE_ENV === "production"
            ? config.build.assetsSubDirectory // 生产环境下的资源子目录
            : config.dev.assetsSubDirectory; // 开发环境下的资源子目录

    // 使用 path.posix.join 拼接路径，以确保路径的格式在不同操作系统上兼容
    return path.posix.join(assetsSubDirectory, _path);
};

// 生成 CSS 加载器配置
exports.cssLoaders = function (options) {
    options = options || {}; // 如果没有传入 options，则默认是空对象

    // 基本的 CSS 加载器配置
    const cssLoader = {
        loader: "css-loader", // 使用 css-loader 解析 CSS 文件
        options: {
            sourceMap: options.sourceMap, // 是否启用 sourceMap（用于调试）
        },
    };

    // PostCSS 加载器配置
    const postcssLoader = {
        loader: "postcss-loader", // 使用 postcss-loader 处理 CSS（如自动添加前缀）
        options: {
            sourceMap: options.sourceMap, // 是否启用 sourceMap（用于调试）
        },
    };

    // 生成加载器字符串，包含 CSS 加载器和其他可能的样式加载器
    function generateLoaders(loader, loaderOptions) {
        // 如果启用了 postCSS，则将 postCSS 加载器加入到加载器数组中
        const loaders = options.usePostCSS
            ? [cssLoader, postcssLoader]
            : [cssLoader];

        // 如果传入了其他加载器（如 sass, less 等），则添加相应的加载器
        if (loader) {
            loaders.push({
                loader: loader + "-loader", // 使用指定的样式加载器（如 sass-loader, less-loader 等）
                options: Object.assign({}, loaderOptions, {
                    sourceMap: options.sourceMap, // 是否启用 sourceMap
                }),
            });
        }

        // 如果需要提取 CSS（例如在生产环境下），使用 ExtractTextPlugin 提取 CSS 到独立文件
        if (options.extract) {
            return ExtractTextPlugin.extract({
                use: loaders, // 使用的加载器
                fallback: "vue-style-loader", // 回退使用 vue-style-loader
            });
        } else {
            // 否则，返回内联的 vue-style-loader 和其他加载器
            return ["vue-style-loader"].concat(loaders);
        }
    }

    // 返回一个包含不同样式格式加载器的对象
    return {
        css: generateLoaders(), // 默认 CSS 加载器
        postcss: generateLoaders(), // 使用 postcss 加载器处理的 CSS
        less: generateLoaders("less"), // 处理 less 格式的文件
        sass: generateLoaders("sass", { indentedSyntax: true }), // 处理 sass 格式的文件，启用缩进语法
        scss: generateLoaders("sass"), // 处理 scss 格式的文件
        stylus: generateLoaders("stylus"), // 处理 stylus 格式的文件
        styl: generateLoaders("stylus"), // styl 文件的处理（stylus 的别名）
    };
};

// 生成独立的样式文件加载器（用于 .vue 文件之外的样式文件）
exports.styleLoaders = function (options) {
    const output = []; // 存储样式加载器的数组
    const loaders = exports.cssLoaders(options); // 获取 CSS 加载器

    // 遍历每种样式格式，生成对应的 loader 配置
    for (const extension in loaders) {
        const loader = loaders[extension];
        output.push({
            test: new RegExp("\\." + extension + "$"), // 根据文件扩展名匹配文件
            use: loader, // 使用相应的加载器
        });
    }

    return output; // 返回加载器数组
};

// 创建通知回调函数，用于构建时显示错误通知
exports.createNotifierCallback = () => {
    const notifier = require("node-notifier"); // 引入 node-notifier 模块，用于发送桌面通知

    return (severity, errors) => {
        if (severity !== "error") return; // 仅在错误时发送通知

        const error = errors[0]; // 获取第一个错误
        const filename = error.file && error.file.split("!").pop(); // 获取错误文件名

        // 发送通知
        notifier.notify({
            title: packageConfig.name, // 通知标题为项目名称
            message: severity + ": " + error.name, // 通知内容包含错误级别和错误名称
            subtitle: filename || "", // 如果有文件名，显示文件名
            icon: path.join(__dirname, "logo.png"), // 设置通知图标
        });
    };
};
