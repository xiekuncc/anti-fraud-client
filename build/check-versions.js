"use strict";

// 引入所需的模块
const chalk = require("chalk"); // 用于在终端输出带颜色的文本
const semver = require("semver"); // 用于处理和比较版本号
const packageConfig = require("../package.json"); // 读取 package.json 文件
const shell = require("shelljs"); // 用于执行 shell 命令

// exec 函数，用于同步执行 shell 命令并返回输出
function exec(cmd) {
    return require("child_process").execSync(cmd).toString().trim(); // 执行命令并返回结果
}

// 定义版本要求的数组，初始包含 node 版本的要求
const versionRequirements = [
    {
        name: "node", // 版本检查项名称
        currentVersion: semver.clean(process.version), // 当前 Node.js 版本
        versionRequirement: packageConfig.engines.node, // package.json 中指定的 Node.js 版本要求
    },
];

// 检查是否安装了 npm，如果安装了，则也检查 npm 的版本
if (shell.which("npm")) {
    versionRequirements.push({
        name: "npm", // 版本检查项名称
        currentVersion: exec("npm --version"), // 当前 npm 版本
        versionRequirement: packageConfig.engines.npm, // package.json 中指定的 npm 版本要求
    });
}

// 导出一个函数，用于检查版本是否满足要求
module.exports = function () {
    const warnings = []; // 用于存储版本不满足要求的警告信息

    // 遍历所有版本要求
    for (let i = 0; i < versionRequirements.length; i++) {
        const mod = versionRequirements[i]; // 当前的模块（如 node 或 npm）

        // 如果当前版本不符合要求，添加警告信息
        if (!semver.satisfies(mod.currentVersion, mod.versionRequirement)) {
            warnings.push(
                mod.name +
                    ": " +
                    chalk.red(mod.currentVersion) +
                    " should be " + // 当前版本为红色
                    chalk.green(mod.versionRequirement), // 要求版本为绿色
            );
        }
    }

    // 如果有警告，输出警告并终止进程
    if (warnings.length) {
        console.log("");
        console.log(
            chalk.yellow(
                "To use this template, you must update following to modules:",
            ),
        ); // 输出警告标题
        console.log();

        // 输出每个警告
        for (let i = 0; i < warnings.length; i++) {
            const warning = warnings[i];
            console.log("  " + warning);
        }

        console.log();
        process.exit(1); // 终止进程并返回错误码 1，表示版本不满足要求
    }
};
