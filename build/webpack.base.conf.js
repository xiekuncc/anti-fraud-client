// 'use strict' 模式可以提高代码的安全性，帮助捕获一些常见的编码错误
"use strict";

// 导入必要的模块
const path = require("path");
const utils = require("./utils");
const config = require("../config");
const vueLoaderConfig = require("./vue-loader.conf");

// 解析路径的辅助函数
function resolve(dir) {
    return path.join(__dirname, "..", dir);
}

// 创建 ESLint 规则的辅助函数
const createLintingRule = () => ({
    test: /\.(js|vue)$/, // 匹配 .js 和 .vue 文件
    loader: "eslint-loader", // 使用 ESLint 加载器
    enforce: "pre", // 在其他加载器之前运行
    include: [resolve("src"), resolve("test")], // 包括 src 和 test 目录
    options: {
        formatter: require("eslint-friendly-formatter"), // 使用友好的格式化器
        emitWarning: !config.dev.showEslintErrorsInOverlay, // 如果不在覆盖层中显示 ESLint 错误，则发出警告
    },
});

module.exports = {
    // 设置上下文目录
    context: path.resolve(__dirname, "../"),

    // 入口点配置
    entry: {
        app: "./src/main.js", // 主入口文件
    },

    // 输出配置
    output: {
        path: config.build.assetsRoot, // 输出目录
        filename: "[name].js", // 输出文件名
        publicPath:
            process.env.NODE_ENV === "production"
                ? config.build.assetsPublicPath // 生产环境下的公共路径
                : config.dev.assetsPublicPath, // 开发环境下的公共路径
    },

    // 模块解析配置
    resolve: {
        extensions: [".js", ".vue", ".json"], // 自动解析确定的扩展
        alias: {
            vue$: "vue/dist/vue.esm.js", // 别名配置，指向 Vue 的 ESM 版本
            "@": resolve("src"), // 别名配置，@ 指向 src 目录
        },
    },

    // 模块规则配置
    module: {
        rules: [
            // ...(config.dev.useEslint ? [createLintingRule()] : []), // 根据配置决定是否使用 ESLint 规则
            {
                test: /\.vue$/, // 匹配 .vue 文件
                loader: "vue-loader", // 使用 Vue 加载器
                options: vueLoaderConfig, // 加载器选项
            },
            {
                test: /\.js$/, // 匹配 .js 文件
                loader: "babel-loader", // 使用 Babel 加载器
                include: [
                    resolve("src"),
                    resolve("test"),
                    resolve("node_modules/webpack-dev-server/client"),
                ], // 包括的目录
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/, // 匹配图片文件
                loader: "url-loader", // 使用 URL 加载器
                options: {
                    limit: 10000, // 小于 10KB 的文件会被转为 base64 编码
                    name: utils.assetsPath("img/[name].[hash:7].[ext]"), // 文件名格式
                },
            },
            {
                test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/, // 匹配媒体文件
                loader: "url-loader", // 使用 URL 加载器
                options: {
                    limit: 10000, // 小于 10KB 的文件会被转为 base64 编码
                    name: utils.assetsPath("media/[name].[hash:7].[ext]"), // 文件名格式
                },
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/, // 匹配字体文件
                loader: "url-loader", // 使用 URL 加载器
                options: {
                    limit: 10000, // 小于 10KB 的文件会被转为 base64 编码
                    name: utils.assetsPath("fonts/[name].[hash:7].[ext]"), // 文件名格式
                },
            },
        ],
    },

    // Node.js 模块配置
    node: {
        // 防止 Webpack 注入无用的 setImmediate 多任务处理程序，因为 Vue 源代码中已经包含了它
        setImmediate: false,

        // 防止 Webpack 注入对 Node 原生模块的模拟，因为这些模拟对客户端没有意义
        dgram: "empty",
        fs: "empty",
        net: "empty",
        tls: "empty",
        child_process: "empty",

        // 保留 process 对象
        process: true,
    },
};
