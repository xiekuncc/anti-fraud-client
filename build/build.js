"use strict";

// 引入并执行版本检查文件，确保 Node 和 npm 的版本满足要求
require("./check-versions")();

// 设置 Node 环境为生产模式
process.env.NODE_ENV = "production";

// 引入相关的模块
const ora = require("ora"); // ora 用于显示命令行中的 loading 动画
const rm = require("rimraf"); // rimraf 用于删除文件和文件夹
const path = require("path"); // path 模块用于处理文件路径
const chalk = require("chalk"); // chalk 用于为命令行输出添加颜色
const webpack = require("webpack"); // webpack 核心模块
const config = require("../config"); // 引入项目配置文件
const webpackConfig = require("./webpack.prod.conf"); // 引入生产环境下的 webpack 配置文件

// 创建一个 loading 动画，提示正在构建生产版本
const spinner = ora("正在为生产环境构建...");
spinner.start();

// 删除构建目录下的旧文件
rm(
    path.join(config.build.assetsRoot, config.build.assetsSubDirectory),
    (err) => {
        if (err) throw err; // 如果删除过程中发生错误，则抛出异常

        // 使用 webpack 执行构建
        webpack(webpackConfig, (err, stats) => {
            spinner.stop(); // 停止 loading 动画

            if (err) throw err; // 如果构建过程中发生错误，抛出异常

            // 将 webpack 构建的结果输出到控制台
            process.stdout.write(
                stats.toString({
                    colors: true, // 启用彩色输出
                    modules: false, // 不输出模块信息
                    children: false, // 如果使用 ts-loader，设置为 true 可以显示 TypeScript 错误
                    chunks: false, // 不显示 chunks 信息
                    chunkModules: false, // 不显示 chunk 中的模块信息
                }) + "\n\n",
            );

            // 如果构建中有错误，则输出错误信息并退出程序
            if (stats.hasErrors()) {
                console.log(chalk.red("  构建失败，出现错误。\n"));
                process.exit(1);
            }

            // 如果构建成功，输出成功信息
            console.log(chalk.cyan("  构建完成。\n"));
            console.log(
                chalk.yellow(
                    "  提示：构建后的文件需要通过 HTTP 服务器来提供服务。\n" +
                        "  通过 file:// 打开 index.html 是无法正常工作的。\n",
                ),
            );
        });
    },
);
