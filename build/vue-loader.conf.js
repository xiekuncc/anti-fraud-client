// 'use strict' 模式可以提高代码的安全性，帮助捕获一些常见的编码错误
"use strict";

// 导入工具函数模块
const utils = require("./utils");
// 导入配置文件模块
const config = require("../config");

// 判断当前环境是否为生产环境
const isProduction = process.env.NODE_ENV === "production";

// 根据当前环境决定是否启用源代码映射
const sourceMapEnabled = isProduction
    ? config.build.productionSourceMap
    : config.dev.cssSourceMap;

// 导出配置对象
module.exports = {
    // 配置 CSS 加载器
    loaders: utils.cssLoaders({
        sourceMap: sourceMapEnabled, // 启用源代码映射
        extract: isProduction, // 生产环境下提取 CSS 文件
    }),
    cssSourceMap: sourceMapEnabled, // 是否启用 CSS 源代码映射
    cacheBusting: config.dev.cacheBusting, // 是否启用缓存破坏
    transformToRequire: {
        video: ["src", "poster"], // 将 <video> 标签的 src 和 poster 属性转换为 require() 调用
        source: "src", // 将 <source> 标签的 src 属性转换为 require() 调用
        img: "src", // 将 <img> 标签的 src 属性转换为 require() 调用
        image: "xlink:href", // 将 <image> 标签的 xlink:href 属性转换为 require() 调用
    },
};
