// 引入 PostCSS 配置的示例 URL
// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
    plugins: {
        "postcss-import": {}, // 允许你在 CSS 中使用 @import 语句来引入其他 CSS 文件
        "postcss-url": {}, // 处理 URL，例如将相对路径转换为绝对路径或内联小文件
        // to edit target browsers: use "browserslist" field in package.json
        // 注释：要编辑目标浏览器，可以在 package.json 文件中使用 "browserslist" 字段
        autoprefixer: {}, // 自动为 CSS 添加浏览器前缀，以确保兼容性
    },
};
