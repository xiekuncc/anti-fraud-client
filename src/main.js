import Vue from "vue";
import App from "./App";
import router from "./router";
import store from "./store/index";
import "./assets/css/index.scss";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "@/assets/js/iconfont.js";
import "@/assets/js/iconfont1.js";
import "@/assets/js/iconfont2.js";
import "@/assets/js/iconfont3.js";
import VueMarkdown from "vue-markdown";

// 文档渲染组件
import showdown from "showdown";
// 视频组件
import videojs from "video.js";

import hljs from "highlight.js";
import "highlight.js/styles/googlecode.css";

// 图片裁剪组件
import VueCropper from "vue-cropper";

// 富文本编辑器
import { mavonEditor } from "mavon-editor";
import "mavon-editor/dist/css/index.css";
Vue.prototype.$videoJS = videojs; // 样式文件
Vue.directive("highlight", function (el) {
    let blocks = el.querySelectorAll("pre code");
    blocks.forEach((block) => {
        hljs.highlightBlock(block);
    });
});

// 全局注册组件
Vue.use(ElementUI);
Vue.use(showdown);
Vue.use(VueCropper);
Vue.use(mavonEditor);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    el: "#app",
    router,
    store,
    components: {
        App,
        VueMarkdown,
    },
    template: "<App/>",
});
