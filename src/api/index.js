import { get, post } from "./http";

// ============文章相关================
// 根据条件查询全部文章（分页）
export const getAllArticle = (params) => post(`/article/getList`, params);
// 根据标签查询文章（分页）
export const getListByTag = (params) => post(`/article/getListByTag`, params);
// 根据文章id查询文章
export const articleById = (id) => get(`/article/get?id=${id}`);
// 获取主页列表接口
export const getHomeList = () => get(`/article/getHomeList`);
// 获取侧边栏文章
export const getSidebarList = () => get(`/article/getSidebarList`);
// 获取侧边栏视频
export const getSidebarVideoList = () => get(`/article/getSidebarVideoList`);
// 获取菜单下的文章列表
export const getMenuArticleList = (id) =>
    get(`/article/getMenuArticleList?id=${id}`);
// 根据文章id获取题目
export const getQuestion = (id) => get(`/article/getQuestion?id=${id}`);
// 每日一练
export const everydayExercise = () => get(`/article/everydayExercise`);

// ============用户相关================
// 注册
export const SignUp = (params) => post(`/user/register`, params);
// 发送验证码
export const sendCode = (email) =>
    get(`/user/getVerificationCode?email=${email}`);
// 登录
export const loginIn = (params) => post(`/login`, params);
// 登出
export const logout = (params) => post(`/logout`, params);
// 获取登录信息
export const getUserInfo = () => get(`/user/userInfo`);
// 更新用户信息
export const updateUserMsg = (params) => post(`/user/update`, params);
// 修改密码
export const updatePassword = (params) => post(`/user/updatePassword`, params);
// 修改密码
export const loginUpdatePassword = (params) =>
    post(`/user/loginUpdatePassword`, params);
// 修改头像
export const updateAvator = (params) => post(`/user/updateAvator`, params);

// ===========评论======================
// 根据文章id获取评论
export const getListByArticleId = (params) =>
    post(`/articleComment/getListByArticleId`, params);
// 提交评论
export const setComment = (params) => post(`/articleComment/comment`, params);
// 删除评论
export const delComment = (id) => post(`/articleComment/deleteById?id=${id}`);

// ===========点赞======================
// 点赞
export const setLike = (params) => post(`/likeRecord/like`, params);
// 取消点赞
export const noLike = (params) => post(`/likeRecord/noLike`, params);
// 获取点赞数量
export const getLikeCount = (id) => get(`/likeRecord/getLikeCount?id=${id}`);

// ===========标签======================
// 获取所有标签
export const getAllTags = () => get(`/tags/getAllTags`);

// ===========配置======================
// 获取配置
export const getConfig = (name) => get(`/config/get?name=${name}`);
// 获取多个配置
export const getConfigList = (params) => post(`/config/getList`, params);
