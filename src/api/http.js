import axios from "axios";
import router from "../router";

axios.defaults.timeout = 10000; // 超时时间是10秒
axios.defaults.withCredentials = true; // 允许跨域
// Content-Type 响应头
axios.defaults.headers.post["Content-Type"] = "application/json";
// 基础url
axios.defaults.baseURL = "http://119.28.160.88:8091/mapi";
// axios.defaults.baseURL = 'http://127.0.0.1:8090/mapi'

// 设置请求拦截器
axios.interceptors.request.use(
    (config) => {
        // 在请求发送前，可以在这里进行一些处理，比如添加认证信息
        const token = localStorage.getItem("accessToken"); // 从LocalStorage中获取token
        if (token) {
            config.headers["accessToken"] = `${token}`;
        }
        return config;
    },
    (error) => {
        // 处理请求错误
        return Promise.reject(error);
    },
);

// 响应拦截器
axios.interceptors.response.use(
    (response) => {
        // 如果reponse里面的code是1，说明访问成功了，否则错误
        if (response.status === 200) {
            return Promise.resolve(response);
        } else if (response.status === 401) {
            console.log("清除");
            // 清除特定项
            localStorage.removeItem("accessToken");
            return Promise.reject(response);
        } else {
            return Promise.reject(response);
        }
    },
    (error) => {
        if (error.response.data.code) {
            switch (error.response.data.code) {
                case 401: // 未登录
                    router.replace({
                        path: "/",
                        query: {
                            redirect: router.currentRoute.fullPath,
                        },
                    });
                    break;
                case 404: // 没找到
                    break;
            }
            return Promise.reject(error.response);
        }
    },
);

/**
 * 封装get方法
 */
export function get(url, params = {}) {
    return new Promise((resolve, reject) => {
        axios
            .get(url, { params: params })
            .then((response) => {
                resolve(response.data);
            })
            .catch((err) => {
                reject(err);
            });
    });
}

/**
 * 封装post方法
 * post统一发json
 */
export function post(url, data = {}) {
    return new Promise((resolve, reject) => {
        axios
            .post(url, data)
            .then((response) => {
                resolve(response.data);
            })
            .catch((err) => {
                reject(err);
            });
    });
}
