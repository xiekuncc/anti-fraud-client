const article = {
    state: {
        listOfArticles: [], // 当前文章列表
        url: "", // 文章地址
        id: "", // 文章id
        title: "", // 标题
        artist: "", // 文章名
        picUrl: "", // 文章图片
    },
    getters: {
        listOfArticles: (state) => {
            let listOfArticles = state.listOfArticles;
            if (!listOfArticles.length) {
                listOfArticles = JSON.parse(
                    window.sessionStorage.getItem("listOfArticles") || null,
                );
            }
            return listOfArticles;
        },
        url: (state) => {
            let url = state.url;
            if (!url) {
                url = JSON.parse(window.sessionStorage.getItem("url") || null);
            }
            return url;
        },
        id: (state) => {
            let id = state.id;
            if (!id) {
                id = JSON.parse(window.sessionStorage.getItem("id") || null);
            }
            return id;
        },
        title: (state) => {
            let title = state.title;
            if (!title) {
                title = JSON.parse(
                    window.sessionStorage.getItem("title") || null,
                );
            }
            return title;
        },
        artist: (state) => {
            let artist = state.artist;
            if (!artist) {
                artist = JSON.parse(
                    window.sessionStorage.getItem("artist") || null,
                );
            }
            return artist;
        },
        picUrl: (state) => {
            let picUrl = state.picUrl;
            if (!picUrl) {
                picUrl = JSON.parse(
                    window.sessionStorage.getItem("picUrl") || null,
                );
            }
            return picUrl;
        },
    },
    mutations: {
        setListOfArticles: (state, listOfArticles) => {
            state.listOfArticles = listOfArticles;
            window.sessionStorage.setItem(
                "listOfArticles",
                JSON.stringify(listOfArticles),
            );
        },
        setUrl: (state, url) => {
            state.url = url;
            window.sessionStorage.setItem("url", JSON.stringify(url));
        },
        setId: (state, id) => {
            state.id = id;
            window.sessionStorage.setItem("id", JSON.stringify(id));
        },
        setTitle: (state, title) => {
            state.title = title;
            window.sessionStorage.setItem("title", JSON.stringify(title));
        },
        setArtist: (state, artist) => {
            state.artist = artist;
            window.sessionStorage.setItem("artist", JSON.stringify(artist));
        },
        setPicUrl: (state, picUrl) => {
            state.picUrl = picUrl;
            window.sessionStorage.setItem("picUrl", JSON.stringify(picUrl));
        },
    },
};

export default article;
