import Vue from "vue";
import Vuex from "vuex";
import configure from "./configure";
import user from "./user";
import article from "./article";

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        configure,
        user,
        article,
    },
});

export default store;
