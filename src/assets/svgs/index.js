import home from "./home.svg";
import all_article from "./all_article.svg";
import study from "./study.svg";
import setting from "./setting.svg";
import my from "./my.svg";
import logout from "./logout.svg";
import moon from "./moon.svg";
import sun from "./sun.svg";
import search from "./search.svg";
import user from "./user.svg";
import unfold from "./unfold.svg";
import sign_up from "./sign_up.svg";
import catalog from "./catalog.svg";
import house from "./house.svg";
import castle from "./castle.svg";
import up_fold from "./up_fold.svg";
import down_unfold from "./down_unfold.svg";
import video from "./video.svg";
import article from "./article.svg";
import question from "./question.svg";
import top from "./top.svg";
import bottom from "./bottom.svg";
import pause from "./pause.svg";
import music from "./music.svg";
import play from "./play.svg";
import green_tick from "./green_tick.svg";
import red_fork from "./red_fork.svg";
import wave from "./wave.svg";
import thumbs_up from "./thumbs_up.svg";
import forward from "./forward.svg";
import collection from "./collection.svg";

export default {
    home,
    all_article,
    study,
    setting,
    my,
    logout,
    moon,
    sun,
    search,
    user,
    unfold,
    sign_up,
    catalog,
    house,
    castle,
    up_fold,
    down_unfold,
    video,
    article,
    question,
    top,
    bottom,
    pause,
    music,
    play,
    green_tick,
    red_fork,
    wave,
    thumbs_up,
    forward,
    collection,
};
