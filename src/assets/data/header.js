// 左侧导航栏
const navMsg = [
    { name: "首页", path: "/", icon: "home" },
    { name: "全部文章", path: "/article-list", icon: "all_article" },
    { name: "学习", path: "/study", icon: "study" },
    { name: "我的资料", path: "/user-info", icon: "my" },
];

// 手机导航栏
const phoneNavMsg = [
    { name: "首页", path: "/", icon: "home" },
    { name: "全部文章", path: "/article-list", icon: "all_article" },
    { name: "学习", path: "/study", icon: "study" },
    { name: "我的资料", path: "/user-info", icon: "my" },
    { name: "设置", path: "/setting", icon: "setting" },
];

// 右侧导航栏
const loginMsg = [
    { name: "登录", path: "/login-in", icon: "user" },
    { name: "注册", path: "/sign-up", icon: "sign_up" },
];

// 用户下拉菜单
const menuList = [
    { name: "设置", path: "/setting", icon: "setting" },
    { name: "退出", path: 0, icon: "logout" },
];

export { navMsg, phoneNavMsg, loginMsg, menuList };
