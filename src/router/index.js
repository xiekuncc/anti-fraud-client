import Vue from "vue";
import Router from "vue-router";
import Home from "@/pages/Home";
import UserInfo from "@/pages/UserInfo";
import Study from "@/pages/Study";
import ArticleList from "@/pages/ArticleList";
import Search from "@/pages/Search";
import SignUp from "@/pages/SignUp";
import LoginIn from "@/pages/LoginIn";
import Setting from "@/pages/Setting";
import ArticleItem from "@/pages/ArticleItem";
import VideoItem from "@/pages/VideoItem";
import Question from "@/pages/Question";
import MenuArticle from "@/pages/MenuArticle";
import LocalVideo from "@/pages/LocalVideo";

Vue.use(Router);

const originalPush = Router.prototype.push;

Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch((err) => err);
};

export default new Router({
    routes: [
        {
            path: "/",
            name: "home",
            component: Home,
        },
        {
            path: "/user-info",
            name: "user-info",
            component: UserInfo,
        },
        {
            path: "/study",
            name: "study",
            component: Study,
        },
        {
            path: "/article-list/:id/:name",
            name: "article-list",
            component: ArticleList,
        },
        {
            path: "/article-list",
            name: "article-list",
            component: ArticleList,
        },
        {
            path: "/search",
            name: "search",
            component: Search,
        },
        {
            path: "/sign-up",
            name: "sign-up",
            component: SignUp,
        },
        {
            path: "/login-in",
            name: "login-in",
            component: LoginIn,
        },
        {
            path: "/setting",
            name: "setting",
            component: Setting,
        },
        {
            path: "/article-item/:id",
            name: "article-item",
            component: ArticleItem,
        },
        {
            path: "/video-item/:id",
            name: "video-item",
            component: VideoItem,
        },
        {
            path: "/question/:id",
            name: "question",
            component: Question,
        },
        {
            path: "/menu-article/:id",
            name: "menu-article",
            component: MenuArticle,
        },
        {
            path: "/local-video",
            name: "local-video",
            component: LocalVideo,
        },
    ],
    scrollBehavior(to, from, savedPosition) {
        return { x: 0, y: 300 }; // 往下滚动 300px
    },
});
