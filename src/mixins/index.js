import {mapGetters} from "vuex";
import {getAllArticle} from "../api/index";

export const mixin = {
    computed: {
        ...mapGetters([
            "loginIn", // 用户是否已登录
            "userId", // 当前登录用户的id
        ]),
    },
    methods: {
        // 提示信息
        notify(title, type) {
            this.$notify({
                title: title,
                type: type,
            });
        },

        // 获取图片地址
        attachImageUrl(srcUrl) {
            return srcUrl
                ? this.$store.state.configure.HOST + srcUrl
                : this.$store.state.configure.HOST + "/img/user.jpg";
        },
        // 根据文章名字模糊查询文章
        getArticle() {
            if (!this.$route.query.keywords) {
                this.$store.commit("setListOfArticles", []);
                this.notify("您输入的内容为空", "warning");
            } else {
                getAllArticle({
                    keywords: this.$route.query.keywords,
                })
                    .then((res) => {
                        if (!res.length) {
                            this.$store.commit("setListOfArticles", []);
                            this.notify("系统暂无符合条件的文章", "warning");
                        } else {
                            this.$store.commit("setListOfArticles", res);
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }
        },
    },
};
