// 'use strict' 模式可以提高代码的安全性，帮助捕获一些常见的编码错误
"use strict";

// 导入必要的模块
const path = require("path");

module.exports = {
    dev: {
        headers: { "Access-Control-Allow-Origin": "*" }, // 设置 CORS 头，允许跨域请求
        // 定义了静态资源（如图片、样式表等）在输出目录中的子目录
        assetsSubDirectory: "static",
        // 定义了静态资源（如图片、样式表等）在输出目录中的公共路径
        assetsPublicPath: "/",
        proxyTable: {}, // 代理表配置，用于解决开发环境下的跨域问题
        hot: true, // 启用热更新
        open: true, // 配置自动启动浏览器

        // 各种开发服务器设置
        host: "0.0.0.0", // 可以被 process.env.HOST 覆盖
        port: 8081, // 可以被 process.env.PORT 覆盖，如果端口被占用，会自动选择一个空闲端口
        autoOpenBrowser: false, // 自动打开浏览器
        errorOverlay: true, // 在页面上显示错误信息
        notifyOnErrors: true, // 出现错误时发送通知
        poll: false, // 文件系统轮询检查文件变化，默认为 false

        // 定义是否在构建期间进行代码规范检查
        useEslint: true,
        showEslintErrorsInOverlay: false, // 是否在页面上显示 ESLint 错误
        devtool: "cheap-module-eval-source-map", // 开发环境下的 source map 类型
        cacheBusting: true, // 是否启用缓存破坏
        cssSourceMap: true, // 是否启用 CSS 源代码映射
        // proxy: 'http://127.0.0.1:8080', // 代理配置，用于解决开发环境下的跨域问题
    },

    build: {
        // 模板文件路径
        index: path.resolve(__dirname, "../dist/index.html"),

        // 路径配置
        assetsRoot: path.resolve(__dirname, "../dist"), // 构建输出的根目录
        assetsSubDirectory: "static", // 静态资源子目录
        assetsPublicPath: "/", // 静态资源公共路径

        /**
         * Source Maps
         */
        productionSourceMap: true, // 是否启用生产环境的源代码映射
        // 生产环境下的 source map 类型
        devtool: "#source-map",

        // Gzip 压缩配置
        productionGzip: false, // 是否启用 Gzip 压缩，默认关闭
        productionGzipExtensions: ["js", "css"], // 需要压缩的文件类型

        // Bundle Analyzer 报告配置
        bundleAnalyzerReport: process.env.npm_config_report, // 是否生成 bundle 分析报告
    },
};
